import os
import time
import pyautogui


import pymysql.cursors
from mysql.connector import errorcode
from selenium import webdriver
import pyperclip  



class WhatsAppBot:

    def __init__(self):
        #options = webdriver.ChromeOptions()
        #options.add_argument('lang=pt-br')
        #self.driver = webdriver.Chrome(executable_path=r'./chromedriver.exe')
        #self.driver.maximize_window()
        self.DB_HOST = 'sql10.freesqldatabase.com'
        self.DB_PORT = '3306'
        self.DB_DATABASE = 'sql10610901'
        self.DB_USERNAME = 'sql10610901'
        self.DB_PASSWORD = 'xGpQzbk59c'

    def enviarMensagens(self, pagina):
        if pagina == 'iniciar':
            time.sleep(20)
            try:
                conexao = pymysql.connect(
                    user=self.DB_USERNAME,
                    password=self.DB_PASSWORD,
                    host=self.DB_HOST,
                    database=self.DB_DATABASE,
                    cursorclass=pymysql.cursors.DictCursor)
            except mysql.connector.Error as err:
                if err.errno == errorcode.ER_ACCESS_DENIED_ERROR:
                    print("Usuário ou Senha esta incorrto,verifique")
                elif err.errno == errorcode.ER_BAD_DB_ERROR:
                    print("Banco de Dados não Existe, verifique.")
                else:
                    print(err)
            else:
                print("")

            try:

                cursor = conexao.cursor()
                sqlConfiguracoes = "select ctt.id, ctt.nome, ctt.telefone, c.mensagem from sql10610901.campanha c, sql10610901.campanha_contato cc, " \
                                   "sql10610901.contato ctt where c.id = cc.campanha_id and cc.contato_id = ctt.id and ctt.enviado = 0 order by 1"
                cursor.execute(sqlConfiguracoes)
                resultadoConfiguracoes = cursor.fetchall()
                cursor.close()

                for rowConfiguracoes in resultadoConfiguracoes:
                    nome = rowConfiguracoes['nome']
                    telefone = rowConfiguracoes['telefone']
                    mensagem = rowConfiguracoes['mensagem']
                    id = rowConfiguracoes['id']

                    print("----- Enviando mensagem para "+nome+" -----")

                    try:
                        img = pyautogui.locateCenterOnScreen('acao1.png', confidence=0.9)
                        pyautogui.click(img.x, img.y)
                        time.sleep(2)
                    except:
                        img = pyautogui.locateCenterOnScreen('acao1except.png', confidence=0.9)
                        pyautogui.click(img.x, img.y)
                        time.sleep(2)

                    pyperclip.copy('https://api.whatsapp.com/send?phone='+telefone)
                    pyautogui.hotkey('ctrl', 'v')
                    pyautogui.press('enter')
                    time.sleep(3)

                    img = pyautogui.locateCenterOnScreen('acao2.png', confidence=0.9)
                    pyautogui.click(img.x, img.y)
                    time.sleep(2)

                    img = pyautogui.locateCenterOnScreen('acao3.png', confidence=0.9)
                    pyautogui.click(img.x, img.y)
                    time.sleep(17)

                    ### clica no campo texto para envio de mensagem
                    img = pyautogui.locateCenterOnScreen('acao4.png', confidence=0.9)
                    pyautogui.click(img.x, img.y)
                    time.sleep(2)

                    
                    pyperclip.copy(mensagem)
                    pyautogui.hotkey('ctrl', 'v')
                    #pyautogui.press("Return")

                    cursor = conexao.cursor()
                    sqlEnviado = "update sql10610901.contato set enviado = true where id = "+str(id)
                    cursor.execute(sqlEnviado)
                    cursor.close()

                    ### escreve mensagem
                    #pyautogui.typewrite(mensagem, interval=0.1)
                    #pyautogui.typewrite(mensagem)
                    #pyautogui.press("Return")

                

            finally:
                print("Campanha enviada para todos os contatos")
                #time.sleep(10)
                #self.enviarMensagens('iniciar')


bot = WhatsAppBot()
bot.enviarMensagens('iniciar')
